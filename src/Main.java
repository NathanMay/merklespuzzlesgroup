import javax.xml.bind.DatatypeConverter;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class Main {

    private static final String FILE_NAME = "puzzlesOutput.bin";



    public static void main(String[] args) throws Exception {

        ArrayList<byte[]> allChunks = new ArrayList<>();
        ArrayList<byte[]> puzzleArrayList = new ArrayList<>();
        byte[] randomChunk;
        PuzzleMaker pm = new PuzzleMaker();

        for(int i = 0; i < 4096; i++){
            byte[] currentPuzzleEncrypted = pm.createEncryptedPuzzle();
            puzzleArrayList.add(currentPuzzleEncrypted);
        }

        System.out.println("Question 1: Puzzle Generation. " + String.valueOf(puzzleArrayList.size()) + "" +
                " Puzzles have been created and Encrypted.");
        System.out.println("*----------------------------------------------*");

        BinFileReadWriter readWriter = new BinFileReadWriter();
        //Write the generated puzzle list to the file.
        readWriter.writeToFile(FILE_NAME, puzzleArrayList);
        System.out.println("Question 2: Puzzle Storage. Puzzles have been written to a binary file. Name is " + FILE_NAME);
        System.out.println("*----------------------------------------------*");

        //Read these from the generated file.
        allChunks = readWriter.getChunksFromFile(FILE_NAME);
        System.out.println("Question 3: Puzzle Cracking. All puzzles have been loaded in from " + FILE_NAME);
        randomChunk = readWriter.getRandomChunk(allChunks);
        System.out.println("Question 3: Puzzle Cracking. Random puzzle is below (as hex)\n" + CryptoLib.getHexStringRepresentation(randomChunk));

        //Finds the solution to the puzzle
        PuzzleSolver ps = new PuzzleSolver();
        byte[] output = ps.bruteForceCracker(randomChunk);
        System.out.println("Question 3: Puzzle Cracking. Brute Force crack is below (as hex)\n" + CryptoLib.getHexStringRepresentation(output));
        System.out.println("*----------------------------------------------*");


        //Extract the puzzle number from the solution.
        byte[] agreedPuzzleNumber = PuzzleSolver.extractPuzzleNumber(output);
        System.out.println("Question 4: Key Lookup. Puzzle Number:  " + CryptoLib.getHexStringRepresentation(agreedPuzzleNumber));
        //Lookup the corresponding Key with this number, getting the Hex Representation.
        String puzzleKey = ps.lookupKey(agreedPuzzleNumber);
        System.out.println("Question 4: Key Lookup. Corresponding Puzzle Key (as String): " + puzzleKey);
        System.out.println("*----------------------------------------------*");


        System.out.println("Question 5: Key Exchange. Bob sends puzzle number " + CryptoLib.getHexStringRepresentation(agreedPuzzleNumber)+ " to Alice.");

        //Convert from mapped string to byte array
        byte[] AlicePuzzleKeyArray = DatatypeConverter.parseHexBinary(puzzleKey);
        String AlicePuzzleMessage = "Encryption Is Magic";

        byte[] AlicePuzzleMessageArray = AlicePuzzleMessage.getBytes(Charset.forName("UTF-8"));
        byte[] AlicesEncryptedMessage = pm.encryptAliceMessage(AlicePuzzleMessageArray, AlicePuzzleKeyArray);
        System.out.println("Question 5: Key Exchange. Alice then sends: " + AlicePuzzleMessage + " : to Bob.");
        System.out.println("Question 5: Key Exchange. The Encrypted message as hex is: " + CryptoLib.getHexStringRepresentation(AlicesEncryptedMessage));


        byte[] decryptedMessage = ps.decryptAlicesMessage(AlicesEncryptedMessage, agreedPuzzleNumber);
        if(decryptedMessage.length != 0){
            String hexMessage = CryptoLib.getHexStringRepresentation(decryptedMessage);
            String messagePlaintext = ps.hexToUTF8(hexMessage);
            System.out.println("Question 5: Key Exchange. Bob has decrypted Alice's Message: " + hexMessage);
            System.out.println("Question 5: Key Exchange. Bob has found this plaintext message: " + messagePlaintext);
        }else{
            System.out.println("Decryption Failed.");
        }
        System.out.println("*----------------------------------------------*");

    }

}
