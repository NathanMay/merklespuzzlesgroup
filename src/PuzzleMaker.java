import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

public class PuzzleMaker {


    private final int NUMBER_OF_PUZZLES = 10;
    private byte zeroByte = 0;
    private Cipher mEncryptor;

    private SecretKey mDES_KEY;
    private static ArrayList<byte[]> mOriginalsList;
    public static Map<String, String> mPuzzleMap;
    public static Map<byte[], byte[]> mPuzzleMapBytes;

    public PuzzleMaker(){
        mPuzzleMap = new HashMap<>();
        mPuzzleMapBytes = new HashMap<>();
    }


    public byte[] createEncryptedPuzzle() throws Exception {

        mEncryptor = Cipher.getInstance("DES");
        byte[] prefix = get16ZeroBytes();
        byte[] puzzleNumber = getPuzzleNumber();
        byte[] bytekey = getRandom8Bytes();
        byte[] desKey = getDESByteArray();

        //Write all arrays to a ByteArrayOutputStream so they can be appended.
        ByteArrayOutputStream oStream = new ByteArrayOutputStream();
        oStream.write(prefix);
        oStream.write(puzzleNumber);
        oStream.write(bytekey);

        //Add to the puzzle map
        mPuzzleMap.put(CryptoLib.getHexStringRepresentation(puzzleNumber), CryptoLib.getHexStringRepresentation(bytekey));
        mPuzzleMapBytes.put(puzzleNumber, bytekey);
        //Create the puzzle byte array via the ByteArrayOutputStream
        byte[] puzzle = oStream.toByteArray();

        //Create the key
        mDES_KEY = CryptoLib.createKey(desKey);

        byte[] encryptedPuzzle = encryptByteArray(puzzle);

        return encryptedPuzzle;
    }
    /**
     * Returns a 2 byte number between 0 and 65535
     */
    private byte[] getPuzzleNumber() {
        Random rand = new Random();
        int randomNumber = rand.nextInt(65535 - 1 + 1) + 1;
        return CryptoLib.smallIntToByteArray(randomNumber);
    }
    /**
     * Returns an array of 8 random bytes
     */
    private byte[] getRandom8Bytes(){
        byte[] array = new byte[8];
        Random rand = new Random();
        //Put random bytes into array

        rand.nextBytes(array);

        return array;
    }
    /**
     * Creates 16 0 bytes for padding the message
     */
    private byte[] get16ZeroBytes(){
        byte[] start = new byte[16];
        //Fill the array with 0 values
        Arrays.fill(start, (byte) 0);
        return start;
    }
    private byte[] getDESByteArray(){
        byte[] random8 = getRandom8Bytes();
        //Final 6 are 0 => keep first two
        for(int i = 2; i < random8.length; i++){
            random8[i] = 0;
        }
        return random8;
    }
    private void printByteArray(byte[] array){
        int arrayLength = array.length;
        System.out.print(String.valueOf(array.length) + " byte array: ");
        for(int i = 0; i < array.length; i++){
            System.out.print(String.valueOf(array[i] & 0xff) + " ");
        }
        System.out.println("");
    }
    public byte[] encryptByteArray(byte[] inputPlaintext) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        mEncryptor.init(Cipher.ENCRYPT_MODE, mDES_KEY);
        byte[] outputCiphertext = mEncryptor.doFinal(inputPlaintext);
        return outputCiphertext;

    }
    /**Encrypt Alice's message using hex encoding*/
    public byte[] encryptAliceMessage(byte[] input, byte[] key){
        try {
            SecretKey aliceKey = CryptoLib.createKey(key);
            mEncryptor.init(Cipher.ENCRYPT_MODE, aliceKey);
            byte[] outputCipher = mEncryptor.doFinal(input);
            return outputCipher;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
