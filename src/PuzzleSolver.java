import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
import sun.rmi.server.InactiveGroupException;

import javax.crypto.*;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

public class PuzzleSolver {

    private Cipher mDecryptor;
    private final int MAX_KEY = 65536;
    private ArrayList<byte[]> mSolutionsList;
    private byte[] mSolution = null;
    private int mPuzzleNumber = 0;


    /* Iterate over possible keys to find a brute force solution to encryption */
    public byte[] bruteForceCracker(byte[] inputChunk) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, IOException {
        mSolutionsList = new ArrayList<>();
        SecretKey bruteForceKey;
        byte[] empty = new byte[32];
        mDecryptor = Cipher.getInstance("DES");
        for(int i = 0; i < MAX_KEY; i++){
            bruteForceKey = getPossibleKey(i);
            decryptByteArray(inputChunk, bruteForceKey);

        }

        checkSolutions();
//        byte[] puzzleNumber = extractPuzzleNumber(mSolution);
//        System.out.println("Puzzle Number: " + CryptoLib.getHexStringRepresentation(puzzleNumber));
//        String puzzleKey = lookupKey(puzzleNumber);
//        System.out.println("Corresponding Key: " + puzzleKey);

        return mSolution;
    }



    private void printSolutions() {
        for(int i = 0; i < mSolutionsList.size(); i++){
            System.out.println(CryptoLib.getHexStringRepresentation(mSolutionsList.get(i)));
        }

    }

    private String checkSolutions() {
        for(int i = 0; i < mSolutionsList.size(); i++){
            String hexRep = CryptoLib.getHexStringRepresentation(mSolutionsList.get(i));
            byte[] possibleSolution = mSolutionsList.get(i);
            //Each hex value contains 2 digits.
            //We padded the front of our plaintext with 16 0's before encryption. Therefore:

            //As each hex digit has 4 bits, we are looking for 128 zero bits:
            String first128bits = hexRep.substring(0, 32);
            int zeroCount = 0;
            for(int j = 0; j < first128bits.length(); j++){
                if(first128bits.charAt(j) == '0'){
                    zeroCount++;
                }

            }
            if(zeroCount == first128bits.length()){
                setmSolution(possibleSolution);
                return hexRep;
            }

        }
        return "None found";
    }

    public void decryptByteArray(byte[] outputCiphertext, SecretKey possibleKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException {

        mDecryptor.init(Cipher.DECRYPT_MODE, possibleKey);
        try{
            byte[] outputPlainText = mDecryptor.doFinal(outputCiphertext);
            mSolutionsList.add(outputPlainText);
        }catch(BadPaddingException e){
            //Do nothing
        }


    }

    /*Iterate all possible encryption keys for brute force solution*/
    public SecretKey getPossibleKey(int iterator) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, IOException {
        byte[] intArray  = CryptoLib.smallIntToByteArray(iterator);
        //Fill the zero array
        byte[] zeroArray = new byte[6];
        //Ending in 48 0's
        for(int i = 0; i < zeroArray.length; i++){
            zeroArray[i] = 0;
        }

        ByteArrayOutputStream bOutStream = new ByteArrayOutputStream();
        bOutStream.write(intArray);
        bOutStream.write(zeroArray);

        byte[] possibleKeyArray = bOutStream.toByteArray();
        //System.out.println("Key Check: " + CryptoLib.getHexStringRepresentation(possibleKeyArray));
        SecretKey possibleKey = CryptoLib.createKey(possibleKeyArray);
        return possibleKey;

    }

    public void setmSolution(byte[] mSolution) {
        this.mSolution = mSolution;
    }

    public void setmPuzzleNumber(int mPuzzleNumber) {
        this.mPuzzleNumber = mPuzzleNumber;
    }

    public static byte[] extractPuzzleNumber(byte[] solution) {
        byte[] puzzleNumber = new byte[2];
//        for(int i = 0; i < solution.length; i++){
//            System.out.println(String.valueOf(i) + ": " + String.valueOf(solution[i] & 0xff));
//        }
        puzzleNumber[0] = solution[16];
        puzzleNumber[1] = solution[17];
        return puzzleNumber;

    }

    public static byte[] extractPuzzleKey(byte[] solution) {
        byte[] puzzleKey = new byte[8];
        for(int i = 0; i < solution.length; i++){
            puzzleKey[i] = solution[18 + i];
        }
        return puzzleKey;

    }

    public String lookupKey(byte[] puzzleNumber){
        System.out.println("Hashmap is being checked...");
        String hexPuzzleNumber = CryptoLib.getHexStringRepresentation(puzzleNumber);
        Set<String> allKeys = PuzzleMaker.mPuzzleMap.keySet();
        for(String key : allKeys) {
            if(hexPuzzleNumber.equals(key)){
//                System.out.println("Found a corresponding key in the Puzzle Key Map!");
                return PuzzleMaker.mPuzzleMap.get(hexPuzzleNumber);
            }

        }
        //Return a message saying "not found" if the corresponding puzzle key cant be found
        System.out.println("Couldn't find a key for this number.");
        return "NOT FOUND";
   }
    //BOB
    public byte[] decryptAlicesMessage(byte[] encryptedMessage, byte[] agreedPuzzleNumber){

        String hexSelectedKey = lookupKey(agreedPuzzleNumber);
        System.out.println("Bob Has Found the Key: " + hexSelectedKey);
        byte[] selectedKeyAsBytes = DatatypeConverter.parseHexBinary(hexSelectedKey);
        try {
            SecretKey selectedKey = CryptoLib.createKey(selectedKeyAsBytes);
            mDecryptor.init(Cipher.DECRYPT_MODE, selectedKey);
            try{
                byte[] outputPlainText = mDecryptor.doFinal(encryptedMessage);
                return outputPlainText;
            }catch(BadPaddingException e){
                //Do nothing
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            }
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }


        return null;
    }

    public String hexToUTF8(String hexString){
        byte[] bytes = HexBin.decode(hexString);
        String plaintext = null;
        try {
            plaintext = new String(bytes, "UTF-8");
            return plaintext;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
