import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class BinFileReadWriter {

    private int BLOCKSIZE = 32;
    private int NUMBER_OF_CHUNKS = 4096;


    public void writeToFile(String filepath, ArrayList<byte[]> inputArray) throws IOException {

        FileOutputStream fOutStream = new FileOutputStream(filepath);
        DataOutputStream dOutStream = new DataOutputStream(fOutStream);
        for(int i = 0; i < inputArray.size(); i++){
            dOutStream.write(inputArray.get(i));
        }
        fOutStream.close();
    }

    public ArrayList<byte[]> getChunksFromFile(String filepath) throws IOException, ClassNotFoundException {

        ArrayList<byte[]> puzzleBlocks = new ArrayList<>();
        Path puzzlePath = Paths.get(filepath);
        byte[] puzzleContents =  Files.readAllBytes(puzzlePath);

        int fileLength = puzzleContents.length;
        if(fileLength / NUMBER_OF_CHUNKS == BLOCKSIZE){
//            System.out.println("File size is OK.");
//            System.out.print("Number of Blocks: " + String.valueOf(fileLength / BLOCKSIZE));
        }else{
            System.out.println("INCORRECT FILE SIZE");
        }

        for(int i = 0; i < fileLength; i+= BLOCKSIZE){
            //Splits puzzle file contents into 32 byte chunks.
            byte[] chunk = Arrays.copyOfRange(puzzleContents, i, i + BLOCKSIZE);
            puzzleBlocks.add(chunk);
//            System.out.println("CHUNK from " + String.valueOf(i) + " to " + String.valueOf(i + BLOCKSIZE) + ": " + CryptoLib.getHexStringRepresentation(chunk));
        }

        return puzzleBlocks;

    }

    public byte[] getRandomChunk(ArrayList<byte[]> chunkList){
        Random random = new Random();
        //Get a random integer upto max chunk.
        int randomInt = random.nextInt((NUMBER_OF_CHUNKS));
        byte[] randomChunk = chunkList.get(randomInt);
        return randomChunk;

    }
}
